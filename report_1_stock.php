<?php
  require_once("mysql.php");

  //find last years
  $q = "SELECT max(year) as max_year FROM qdata";
  $res = $conn->query($q);
  $actor = $res->fetch_assoc();
  $max_year = $actor['max_year'];
  $prev_max_year = $max_year-1;

  //get all stock names
  $q = "SELECT DISTINCT stock_name FROM qdata";
  $res = $conn->query($q);
  $stock_name_arr = [];
  while ($actor = $res->fetch_assoc()) {
    array_push($stock_name_arr,$actor['stock_name']);
  }
  //print_br($stock_name_arr);


  //get data by stock name
  $stock_name = 'ktb';
  $q = "SELECT * FROM qdata WHERE stock_name LIKE '$stock_name' AND (year LIKE '$max_year' OR year LIKE '$prev_max_year') ORDER BY year,quater";
  //print $q;
  $x = [];
  $qdata = [];
  $qdata[$max_year] = [];
  $qdata[$prev_max_year] = [];
  $res = $conn->query($q);
  while ($actor = $res->fetch_assoc()) {
    //array_push($x,$actor);
    $year = $actor['year'];
    $q = $actor['quater'];
    $profit = $actor['stock_profit'];
    $one_data['quater'] = $q;
    $one_data['profit'] = $profit;

    if ($year == $prev_max_year)
      array_push($qdata[$prev_max_year],$one_data);
    if ($year == $max_year)
      array_push($qdata[$max_year],$one_data);
  }
    //print_br($qdata);
  //gen q4
    //prev year
  $year = $prev_max_year;
  $sum_q1_to_q3 = 0;
  $sum_q1_to_q3 += $qdata[$year][0]['profit'];
  $sum_q1_to_q3 += $qdata[$year][1]['profit'];
  $sum_q1_to_q3 += $qdata[$year][2]['profit'];
  $total_profit = 0;
  $qprofit = [];
  for ($i=0;$i<4;$i++) {
    $q = $qdata[$year][$i]['quater'];
    if ($q == 'total')
      $total_profit = $qdata[$year][$i]['profit'];
    else
      array_push($qprofit,$qdata[$year][$i]['profit']);
  }
  $profit_q1_q3 = $qprofit[0]+$qprofit[1]+$qprofit[2];
  $qdata[$year][4]['quater'] = 4;
  $qdata[$year][4]['profit'] = $total_profit - $profit_q1_q3;

    //this year
  $year = $max_year;
  $count_present_year = count($qdata[$max_year]);
  if ($count_present_year == 4) {
    $total = $qdata[$year][3]['profit'];
    $sum_q1_to_q3 = $qdata[$year][0]['profit'] + $qdata[$year][1]['profit'] + $qdata[$year][2]['profit'];
    $qdata[$year][4]['quater'] = 4;
    $qdata[$year][4]['profit'] = $total - $sum_q1_to_q3;
  }

  //print_br($qdata);

  //generate report
    //this year
  $compare_with_prev_quater = [];
  $year = $max_year;
  $year_count = count($qdata[$year]);
    //data not full of year
  for ($i=0;$i<$year_count;$i++) {
    if ($qdata[$year][$i]['quater'] == 'total')
      continue;
    if ($i==0) {
      $compare_with_prev_quater[1] = $qdata[$year][0]['profit'] - $qdata[$prev_max_year][4]['profit'];
      //convert to %
      $compare_with_prev_quater[1] = $compare_with_prev_quater[1]/$qdata[$prev_max_year][4]['profit']*100;
    } else {
      $compare_with_prev_quater[$i+1] = $qdata[$year][$i]['profit'] - $qdata[$year][$i-1]['profit'];
      //convert to %
      $compare_with_prev_quater[$i+1] = $compare_with_prev_quater[$i+1]/$qdata[$year][$i-1]['profit']*100;
    }
  }
    //fix q
  if (isset($compare_with_prev_quater[5])) {
    $compare_with_prev_quater[4] = $compare_with_prev_quater[5];
    unset($compare_with_prev_quater[5]);
  }

  //compare with prev year
  $year = $max_year;
  $compare_with_prev_year = [];
  $year_count = count($qdata[$year]);
  for ($i=0;$i<$year_count;$i++) {
    if ($qdata[$year][$i]['quater'] == 'total')
      continue;
    $compare_with_prev_year[$i+1] = $qdata[$max_year][$i]['profit'] - $qdata[$prev_max_year][$i]['profit'];
    //convert to %
    $compare_with_prev_year[$i+1] = $compare_with_prev_year[$i+1]/$qdata[$prev_max_year][$i]['profit']*100;
  }
    //fix wrong q
  if (isset($compare_with_prev_year[5])) {
    $compare_with_prev_year[4] = $compare_with_prev_year[5];
    unset($compare_with_prev_year[5]);
  }

  //print_br($compare_with_prev_year);
  //print_br($qdata);

  function print_br($arr) {
    print("<pre>".print_r($arr,true)."</pre>");
  }

  function print_percent($num) {
    return number_format($num,2,'.','')."%";
  }

?>

<html>
  <head>
    <style>
      table {
        border-collapse: collapse;
      }

      table, th, td {
        border: 1px solid black;
      }
    </style>
  </head>
  <body>
    <table width="80%" border=1>
      <tr>
        <td><?=$stock_name?></td>
        <td>
          <table width="100%" border=1>
            <tr>
              <td align="center">การเติบโตเมื่อเทียบกับไตรมาสที่แล้ว</td>
            </tr>
            <tr>
              <table width="100%" border=1>
                <tr>
                  <td>Q1</td>
                  <td>Q2</td>
                  <td>Q3</td>
                  <td>Q4</td>
                </tr>
                <tr>
                  <td>
                    <?=isset($compare_with_prev_quater[1])?print_percent($compare_with_prev_quater[1]):"No data"?>
                  </td>
                  <td>
                    <?=isset($compare_with_prev_quater[2])?print_percent($compare_with_prev_quater[2]):"No data"?>
                  </td>
                  <td>
                    <?=isset($compare_with_prev_quater[3])?print_percent($compare_with_prev_quater[3]):"No data"?>
                  </td>
                  <td>
                    <?=isset($compare_with_prev_quater[4])?print_percent($compare_with_prev_quater[4]):"No data"?>
                  </td>
                </tr>
              </table>
            </tr>
            <tr><td></td>
              <td>
                <table width="100%">
                  <tr>
                    <td align="center">การเติบโตเมื่อเทียบกับปีที่แล้ว</td>
                  </tr>
                  <tr>
                    <td>
                      <table width="100%" border=1>
                        <tr>
                          <td>Q1</td>
                          <td>Q2</td>
                          <td>Q3</td>
                          <td>Q4</td>
                        </tr>
                        <tr>
                          <td><?=isset($compare_with_prev_year[1])? print_percent($compare_with_prev_year[1]):"No data"?></td>
                          <td><?=isset($compare_with_prev_year[2])? print_percent($compare_with_prev_year[2]):"No data"?></td>
                          <td><?=isset($compare_with_prev_year[3])? print_percent($compare_with_prev_year[3]):"No data"?></td>
                          <td><?=isset($compare_with_prev_year[4])? print_percent($compare_with_prev_year[4]):"No data"?></td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    <br>
  </body>
</html>
